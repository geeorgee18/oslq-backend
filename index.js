const express = require('express')
require('dotenv').config()
const cors = require('cors')
const morgan = require('morgan')
const {dbConnection} = require('./database/conexion_mongo')

const app = express()
dbConnection()

app.use(cors())
app.use(morgan('dev'))
app.use(express.json())
app.use('/api/test', require('./routes/test'))

app.set('port', process.env.PORT || 4000)

app.listen(app.get('port'), ()=>{
    console.log(`Servidor corriendo en el puerto ${app.get('port')}`)
})
