const Test = require('../models/Test')

const crearTest = async(request, response)=>{
    const test = new Test(request.body)
    try {
        const testGuardado = await test.save()
        response.json({
            ok:true,
            test:testGuardado
        })
    } catch (error) {
        console.log(error)
        response.status(500).json({
            ok:false,
            msg:'Hubo un problema a la hora de registrar el test'
        })
    }
}

module.exports = {
    crearTest
}
