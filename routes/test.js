/*
    Rutas del OSLQ
    host + /api/test
*/
const {Router} = require('express')
const {check} = require('express-validator')
const {validarCampos} = require('../middlewares/validarCampos')
const {crearTest} = require('../controllers/test')

const router = Router()

router.post('/',[
    check('question0','La pregunta #1 es obligatoria').not().isEmpty(),
    check('question1','La pregunta #2 es obligatoria').not().isEmpty(),
    check('question2','La pregunta #3 es obligatoria').not().isEmpty(),
    check('question3','La pregunta #4 es obligatoria').not().isEmpty(),
    check('question4','La pregunta #5 es obligatoria').not().isEmpty(),
    check('question5','La pregunta #6 es obligatoria').not().isEmpty(),
    check('question6','La pregunta #7 es obligatoria').not().isEmpty(),
    check('question7','La pregunta #8 es obligatoria').not().isEmpty(),
    check('question8','La pregunta #9 es obligatoria').not().isEmpty(),
    check('question9','La pregunta #10 es obligatoria').not().isEmpty(),
    check('question10','La pregunta #11 es obligatoria').not().isEmpty(),
    check('question11','La pregunta #12 es obligatoria').not().isEmpty(),
    check('question12','La pregunta #13 es obligatoria').not().isEmpty(),
    check('question13','La pregunta #14 es obligatoria').not().isEmpty(),
    check('question14','La pregunta #15 es obligatoria').not().isEmpty(),
    check('question15','La pregunta #16 es obligatoria').not().isEmpty(),
    check('question16','La pregunta #17 es obligatoria').not().isEmpty(),
    check('question17','La pregunta #18 es obligatoria').not().isEmpty(),
    validarCampos
], crearTest)

module.exports = router
