const {Schema, model} = require('mongoose')

const TestSchema = Schema({
    question0:{
        type:String,
        required:true
    },
    question1:{
        type:String,
        required:true
    },
    question2:{
        type:String,
        required:true
    },
    question3:{
        type:String,
        required:true
    },
    question4:{
        type:String,
        required:true
    },
    question5:{
        type:String,
        required:true
    },
    question6:{
        type:String,
        required:true
    },
    question7:{
        type:String,
        required:true
    },
    question8:{
        type:String,
        required:true
    },
    question9:{
        type:String,
        required:true
    },
    question10:{
        type:String,
        required:true
    },
    question11:{
        type:String,
        required:true
    },
    question12:{
        type:String,
        required:true
    },
    question13:{
        type:String,
        required:true
    },
    question14:{
        type:String,
        required:true
    },
    question15:{
        type:String,
        required:true
    },
    question16:{
        type:String,
        required:true
    },
    question17:{
        type:String,
        required:true
    }
})
TestSchema.method('toJSON', function(){
    const {__v, _id, ...object} = this.toObject()
    object.id = _id
    return object
})

module.exports = model('Test', TestSchema)
